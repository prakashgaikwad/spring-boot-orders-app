package com.classpath.springbootorders.service;

import com.classpath.springbootorders.model.Order;
import com.classpath.springbootorders.repository.OrderDAO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class OrderService {

    private OrderDAO orderDAO;

    public Order saveOrder(Order order){
        return this.orderDAO.save(order);
    }

    public Set<Order> fetchAllOrders(){
        return new HashSet<>(this.orderDAO.findAll());
    }

    public Order findById(long orderId){
        return this.orderDAO
                        .findById(orderId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid Order Id"));
    }

    public void deleteOrder(long orderId){
        this.orderDAO.deleteById(orderId);
    }
}