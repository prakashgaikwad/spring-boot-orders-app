package com.classpath.springbootorders.controller;

import com.classpath.springbootorders.model.Order;
import com.classpath.springbootorders.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
public class OrderController {

    private OrderService orderService;

    @PostMapping
    public Order createOrder(@RequestBody Order order){
        return this.orderService.saveOrder(order);
    }

    @GetMapping
    public Set<Order> fetchAllOrders(){
        return this.orderService.fetchAllOrders();
    }

    @GetMapping("/{id}")
    public Order fetchById(@PathVariable("id") long orderId){
        return this.orderService.findById(orderId);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable("id") long orderId){
        this.orderService.deleteOrder(orderId);
    }
}