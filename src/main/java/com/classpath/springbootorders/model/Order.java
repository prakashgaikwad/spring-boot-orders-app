package com.classpath.springbootorders.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import static javax.persistence.GenerationType.AUTO;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "orderId")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    private double price;

    private LocalDate orderDate;

    private String customerName;
}

