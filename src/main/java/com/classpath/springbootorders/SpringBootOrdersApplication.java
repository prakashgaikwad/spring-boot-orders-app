package com.classpath.springbootorders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBootOrdersApplication implements CommandLineRunner {

    @Autowired
    ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootOrdersApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(" Inside the run method .....");
        String[] beanDefinitions = applicationContext.getBeanDefinitionNames();

        for(String beanName: beanDefinitions){
            if(beanName.equals("orderController") || beanName.equals("orderService") || beanName.equals("orderDAO") ) {
                System.out.println("Bean Name: " + beanName);
            }
        }
    }
}
